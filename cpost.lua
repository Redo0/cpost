
local printtable = require("printtable")

print("cpost")

local ifilename = arg[1]
assert(ifilename, "no input filename specified")

local ofilename = arg[2]
assert(ofilename, "no output filename specified")

local ifile = io.open(ifilename, "rb")
local itext = "\n"..ifile:read("*a").."\n"
ifile:close()

------------------------------------------------

local linenumber = 0

local function cassert(cond, err)
	if not cond then
		error(err.."\n".."line number "..linenumber)
	end
end

local stack = {}
local function gettop()
	return stack[#stack]
end
local function topname()
	return stack[#stack].name
end
local function push(name)
	stack[#stack + 1] = {
		name = name,
		stuff = {},
		text = nil,
		startline = linenumber,
	}
	-- print("push "..name)
end
local function pop(tn)
	if tn then
		cassert(topname()==tn, "tried to pop "..tn.." but got "..topname())
	end
	-- print("pop "..topname())
	local v = stack[#stack]
	stack[#stack] = nil
	return v
end
local function append(x)
	if type(x)=="string" then
		push("text")
		gettop().text = x
		append(pop("text"))
	elseif type(x)=="table" then
		table.insert(stack[#stack].stuff, x)
	else
		error("invalid append type "..tostring(x))
	end
end

push("program")

local idx = 1
while idx <= #itext do
	local c = itext:sub(idx, idx)
	local cp = itext:sub(idx-1, idx-1)
	local cn = itext:sub(idx+1, idx+1)
	
	if c=="\n" then
		if topname()=="slashcomment" then
			append(pop("slashcomment"))
		end
		if topname()=="define" and cp~="\\" then
			append(pop("define"))
		end
		
		linenumber = linenumber + 1
		
		append("\n")
		idx = idx + 1
	elseif c=="\r" then
		idx = idx + 1
	elseif c==" " or c=="\t" then
		append(c)
		idx = idx + 1
	elseif topname()=="quote" then
		if c=="\"" then
			append(pop("quote"))
		else
			append(c)
		end
		idx = idx + 1
	elseif c=="/" and cn=="*" then
		push("blockcomment")
		idx = idx + 2
	elseif c=="*" and cn=="/" then
		append(pop("blockcomment"))
		idx = idx + 2
	elseif topname()~="blockcomment" and topname()~="slashcomment" then
		if c=="/" and cn=="/" then
			push("slashcomment")
			idx = idx + 2
		elseif c=="\"" then
			push("quote")
			idx = idx + 1
		elseif c=="(" then
			push("paren")
			idx = idx + 1
		elseif c==")" then
			append(pop("paren"))
			idx = idx + 1
		elseif c=="[" then
			push("bracket")
			idx = idx + 1
		elseif c=="]" then
			append(pop("bracket"))
			idx = idx + 1
		elseif c=="{" then
			push("brace")
			idx = idx + 1
		elseif c=="}" then
			append(pop("brace"))
			
			if topname()=="lua" then
				append(pop("lua"))
			end
			idx = idx + 1
		elseif c:find("[a-zA-Z0-9_]") then
			local name = itext:match("^[a-zA-Z0-9_]+", idx)
			
			push("name")
			append(name)
			append(pop("name"))
			
			if topname()=="greedyeval" then
				append(pop("greedyeval"))
			end
			
			idx = idx + #name
		elseif c=="#" and cn~="#" then
			local dir = itext:match("^[a-zA-Z]+", idx+1)
			local dirlwr = dir:lower()
			
			if dirlwr=="define" then
				push("define")
			elseif dirlwr=="lua" then
				push("lua")
			else
				append("#"..dir)
			end
			
			idx = idx + 1 + #dir
		elseif c=="$" then
			push("greedyeval")
			idx = idx + 1
		else -- random chars
			append(c)
			-- print("append "..c.." to "..topname())
			-- cassert(false, "invalid char \""..c.."\"")
			idx = idx + 1
		end
	else -- in comment
		append(c)
		idx = idx + 1
	end
end

local final = pop("program")

-------------------------------------------------

math.round = function(x) return math.floor(x + 0.5) end

local function trimspace(s)
	s = s:gsub("^[ \t\n]+", "")
	s = s:gsub("[ \t\n]+$", "")
	return s
end

-- program
-- text
-- slashcomment
-- blockcomment
-- paren
-- bracket
-- brace
-- quote
-- name
-- define
-- lua

local macros = {}
local function definemacro(name, val)
	cassert(not macros[name], "redefinition of macro "..name)
	macros[name] = val
end

local function textify(item, greedy)
	linenumber = item.startline
	local outtbl = {}
	local function oappend(s)
		table.insert(outtbl, s)
	end
	
	if item.name=="program" or item.name=="macdef" then
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, greedy))
		end
	elseif item.name=="name" then
		local namet = {}
		for i, v in ipairs(item.stuff) do
			table.insert(namet, textify(v, greedy))
		end
		local name = table.concat(namet)
		
		-- print("evaluate "..name.." greedy "..(greedy and "1" or "0"))
		
		if greedy and macros[name] then
			return textify(macros[name], true)
		else
			return name
		end
	elseif item.name=="greedyeval" then
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, true))
		end
	elseif item.name=="text" then
		oappend(item.text)
	elseif item.name=="slashcomment" then
		
	elseif item.name=="blockcomment" then
		
	elseif item.name=="paren" then
		oappend("(")
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, greedy))
		end
		oappend(")")
	elseif item.name=="bracket" then
		oappend("[")
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, greedy))
		end
		oappend("]")
	elseif item.name=="brace" then
		oappend("{")
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, greedy))
		end
		oappend("}")
	elseif item.name=="quote" then
		oappend("\"")
		for i, v in ipairs(item.stuff) do
			oappend(textify(v, greedy))
		end
		oappend("\"")
	elseif item.name=="define" then
		cassert(item.stuff[2].name=="name", "macro must begin with name, begins with \""..item.stuff[2].name.."\"")
		local catstart = 3
		if item.stuff[3] and item.stuff[3].name=="paren" then
			catstart = 4
		end
		local macdef = {
			name = "macdef",
			stuff = {},
			startline = item.startline,
		}
		for i = catstart, #item.stuff do
			local v = item.stuff[i]
			table.insert(macdef.stuff, v)
		end
		local namet = {}
		for i = 1, catstart-1 do
			table.insert(namet, textify(item.stuff[i], false))
		end
		local name = table.concat(namet)
		name = trimspace(name)
		
		definemacro(name, macdef)
		
		-- print("new macro "..name)
		
		oappend("#define ")
		oappend(name)
		oappend(textify(macdef, false))
	elseif item.name=="lua" then
		local textt = {}
		for i, v in ipairs(item.stuff) do
			table.insert(textt, textify(v, true))
		end
		local text = table.concat(textt)
		text = trimspace(text)
		text = text:gsub("^%{", "")
		text = text:gsub("%}$", "")
		local f = loadstring(text)
		assert(f, "compilation failed for lua eval "..text)
		local fr = f()
		if fr then
			oappend(fr)
		end
	else
		print("unknown item "..item.name)
	end
	
	return table.concat(outtbl)
end

local otext = textify(final, false)

------------------------------------------------

otext = otext:gsub("[\t\n ]+\n", "\n")

local ofile = io.open(ofilename, "wb")
ofile:write(otext)
ofile:close()

print("done")
